const DB = require('./mongoService');
const Logger = require('./logger');
const Promise = require('promise');
const UService = require('./userService');

class ProductService{
    constructor(userService){
        this.Logger = new Logger("event.log", "ProductService");
        this.UService = userService; 
        this.DBInstance = new DB("product")
        this.productDB;

        this.sortDict = {
            "priceHTL": {"price": 1},
            "priceLTH": {"price": -1},
            "nameAZ": {"displayName": 1}
        }

        this.init().then(connection => this.productDB = connection).catch(err => {
            this.logger.write("Fatal Error: "+err);
        })
    }

    init(){
        return new Promise((resolve, reject) => {
            this.DBInstance.connect().then(newConnection => {
                 resolve(newConnection);
            }).catch(err => {
                this.logger.write("Fatal Error: "+err);
                reject(err);
            });
        })
    }

    getProduct(qpid, qsort){
        return new Promise((resolve,reject) => {

            if(qsort == undefined){qsort = "priceHTL"}
            if(!this.verifySortType(qsort)){reject("Invalid qsort")}

            const coll = this.productDB.collection("productDetail");

            switch(qpid){
                case "*":
                    coll.find({}, (err, cursor) => {
                        if(err){reject("Error"+err)}
                        cursor.sort(this.sortDict[qsort]);

                        resolve(cursor.toArray());
                    })

                default:
                    coll.findOne({"pid": qpid}, (err, respData) => {
                        if(err){reject("Error: "+err)}
                        resolve(respData);
                    })
            }
        })
    }

    addProduct(pObj,uid, token){
        return new Promise((resolve, reject) => {
            
            const veri = this.verifyProduct(pObj);
            if(veri !== null){
                console.log(veri)
                reject(veri);
            }else{

                const coll = this.productDB.collection("productDetail");

                this.UService.authUserAndType(uid, 1, token).then(authRes => {
                    if(authRes > 0){
                        if(pObj.pid === undefined || pObj.pid === null){
                            this.genPID().then(newPid => {
                                pObj = {...pObj, "pid":newPid }
                                
                                coll.insertOne(pObj).then(res => {
                                    this.Logger.write("Event: product created pid: "+ pObj.pid);
                                    resolve(pObj);
                                }).catch(err => {reject(err)})
                            }).catch(pidErr => reject(pidErr))
                        }else{
                            coll.insertOne(pObj).then(res => {
                                this.Logger.write("Event: product created pid: "+ pObj.pid);
                                resolve(pObj);
                            }).catch(err => {reject(err)})
                        }
                    }else{ 
                        reject("Error: insuficient priviledge "+authRes)
                    }
                }).catch(err => {reject(err)});
            }
        })
    }

    deleteProduct(pid, uid, token){
        return new Promise((resolve, reject) => {
            const coll = this.productDB.collection("productDetail"); //we can probably consolidate this 
            
            this.UService.authUserAndType(uid, 1, token).then( authRes => {
                coll.deleteOne({"pid": pid}).then(res => {
                    resolve(res);
                }).catch(err => reject(err));
            }).catch(err => {reject(err)});
            
        })
    }

    //generate a pid correlated with barcode 
     genPID(){
        return new Promise((resolve, reject) => {
            this.getProduct("*", "priceHTL").then(res => {
                let pids = res.map(thisItem => {
                    return thisItem.pid;
                });
                let match = false;
                let pid;
                while(!match){
                    pid = Math.floor(100000000 + Math.random() * 900000000);
                    
                    if(pids.includes(pid)){
                        match = false;
                    }else{match = true;}
                }
                resolve(pid);
            }).catch(err => reject(err));
        })
    }


    verifySortType(qsort){
        if(!(qsort in this.sortDict)){
            return false;
        }else{ return true; }
    }

    verifyProduct(pObj){
        var priceREGEX = new RegExp('^([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$');
        var qauntREGEX = new RegExp('^[0-9]+$')

        if(pObj.price === undefined){
            return "price was not defined"
        }else if(pObj.displayName === undefined){
            return "displayName was not defined"
        }else if(!priceREGEX.test(pObj.price)){
            return "invalid price format"
        }else if(pObj.quantity === undefined){
            return "quantity is not defined";
        }else if(!qauntREGEX.test(pObj.quantity)){
            return "invalid quantity"
        }else{return null}
        
    }
}

module.exports = ProductService;