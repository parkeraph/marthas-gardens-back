const admin = require("firebase-admin");
const serviceAccount = require("../marthas-gardens-firebase-adminsdk.json")
const Promise = require('promise')

class firebase{
    constructor(){
       admin.initializeApp({
           credential: admin.credential.cert(serviceAccount),
           databaseURL: "https://marthas-gardens-web.firebaseio.com"
       }) 
    }

    verifyToken(thisUID, thisToken){
        return new Promise((resolve, reject) => {
            admin.auth().verifyIdToken(thisToken).then(decoded=> {
                if(thisUID == decoded.uid){
                    resolve();
                }else{
                    reject("Authentication Failed");
                }
            }).catch(err => {
                reject(err);
            })
        })
    }

    deleteUser(thisUID){
        return new Promise((resolve, reject) => {
           admin.auth().deleteUser(thisUID).then(res => {
               resolve(res);
           }).catch(err => reject(err));
        })
    }
}

module.exports = firebase;