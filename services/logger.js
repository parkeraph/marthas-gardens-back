const FS = require('fs');


class Logger{
    constructor(fileName, prefix){
        this.logPath = "../marthas-gardens-back/logs/"+fileName;
        this.prefix = " ["+prefix+"] ";
    }

    write(msg){
        const dt = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        const data = dt+this.prefix+msg+"\n"     
        console.log(data)
    }
}

module.exports = Logger;