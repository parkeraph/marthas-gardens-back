const MongoClient = require('mongodb').MongoClient;
const Promise = require('promise');
require('dotenv').config()

class DB{

    constructor(dbName) {
        this.uri = process.env.MONGO_CONNECTION_URI;
        this.dbName = dbName;
    }

    connect() {
        return new Promise((resolve, reject) => {
            MongoClient.connect(this.uri, { useNewUrlParser: true }, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(client.db(this.dbName));
                }
            });
        })

    }
}

module.exports = DB;