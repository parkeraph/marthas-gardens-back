const DB = require('./mongoService');
const Logger = require('./logger');
const firebase = require('./firebase')
const Promise = require('promise')

class UserService{

    constructor(){
        this.logger = new Logger("event.log", "UserService");
        this.fbService = new firebase();

        this.DBInstance = new DB("user");
        this.userDB;
        
        this.init().then(connection => this.userDB = connection).catch(err => {
            this.logger.write("Fatal Error: "+err);
        })
    }

    init(){
        return new Promise((resolve, reject) => {
            this.DBInstance.connect().then(newConnection => {
                 resolve(newConnection);
            }).catch(err => {
                this.logger.write("Fatal Error: "+err);
                reject(err)
            });
        })
    }

    //if the quid === uid then permission of 0 is ok
    //if quid !== uid then permission of 2 needed 
    getUser(uid, quid, token){
        return new Promise((resolve, reject) => {
            const coll = this.userDB.collection("userDetail");

            this.authUserAndType(uid, 0, token).then(authRes => {
                
                    if(quid === "*"){
                        if(authRes === 2){
                            coll.find({}, (err, cursor) => {
                                if(err){reject("Error: "+err)}
                                cursor.sort({"displayName": 1});
                                
                                resolve(cursor.toArray());
                            })
                        }
                    }else if(uid === quid){
                        coll.findOne({"uid":quid}, (err, respData) => {
                            if(err){reject("Error: "+err)}
                            resolve(respData);
                        })
                    }else if(uid !== quid){
                        if(authRes === 2){
                            coll.findOne({"uid":quid}, (err, respData) => {
                                if(err){reject("Error: "+err)}
                                resolve(respData);
                            })
                        }else{reject("Error: insuficient priviledge")}
                    }
                
                
            }).catch(authErr => reject(authErr));
        })
    }

    addUser(uObj, uid, token){
        return new Promise( (resolve, reject) => {
             const coll = this.userDB.collection("userDetail");
            
            if(!uObj.uid || !uObj.displayName || !uObj.email){
                reject("Invalid request body");
            }

            if(uid && token && uObj.userType > 0){
                this.authUserAndType(uid, 2, token).catch(authErr => {
                    this.logger.write("write of usertype "+uObj.userType+"failed due to insufficient privs from "+uObj.uid);
                })
            }else{
                uObj.userType = 0;                
            }


            
            coll.insertOne(uObj).then(res => {
                this.logger.write("Event: User "+uObj.uid+" created")
                resolve(uObj)
            }).catch(err => {
                this.logger.write("Error during adduser: "+err)
                reject(err)
            }); //return promise for asynch handling
         })
     }

    deleteUser(quid, uid, token, dbOnly){
        let authType;
        if(quid === uid){authType = 0}else{authType = 1}
        return new Promise((resolve, reject) => {
            this.authUserAndType(uid, authType, token).then(res => {
                const coll = this.userDB.collection("userDetail");

                coll.deleteOne({"uid": quid}).then(delRes => {
                    if(!dbOnly){
                        this.fbService.deleteUser(quid).then(fbDelRes => {
                            this.logger.write("Event: User "+quid+" deleted");
                            resolve(delRes);
                        }).catch(fbDelErr => reject(fbDelErr));
                    }else{
                        this.logger.write("Event: User "+quid+" deleted (dbOnly)");
                        resolve(delRes);
                    }
                }).catch(delErr => reject(delErr));
            }).catch(err => reject(err));
        })
    }



    //type 0: basic permissions
    //type 1: Employee Permissions
    //type 2: Admin Permissions

    //take another look at this    if > provided priv then accept
     authUserAndType(thisUID, verifyType, IdToken){ //returns the user type and confirms the authenticates via token 
         return new Promise((resolve, reject) => {
                if(verifyType > 2 || verifyType == null || verifyType == undefined){
                    reject("authUserType: invalid usertype");
                }

                if(thisUID == undefined || thisUID == null){
                    reject("authUserType: invalid uid");
                }

                
                if(IdToken == undefined || IdToken == null){
                    reject("authUserType: invalid token");
                }

                

                this.fbService.verifyToken(thisUID, IdToken).then((decoded) => {
                    //confirm validity of token/uid pair
                    this.getUserType(thisUID).then(typeRes => {
                        //check type
                        if(typeRes >= verifyType){
                            resolve(typeRes);
                        }else{
                            this.logger.write("Auth Alert: API request made with wrong usertype MAYBE MALITIOUS");
                            reject("User is not of type "+verifyType);
                        }
                    }).catch((typeErr) => {
                        this.logger.write("Auth Alert: "+typeErr);
                        reject("getUserType: "+typeErr);
                    })
                }).catch((authErr) => {
                    this.logger.write("Auth Alert: "+authErr);
                    reject("fbService: "+authErr);
                })
         })
     }

    //DO NOT USE BY ITSELF, MUST AUTHORIZE FIRST    
    getUserType(thisUID){
        return new Promise((resolve, reject) => {
            const coll = this.userDB.collection("userDetail");

            coll.findOne({uid: thisUID}, (err, res) => {
                if(err){
                    reject(err);
                }else if(res == null){
                    reject("User not found");
                }else{
                    resolve(res.userType);
                }
            })
        })
    }



}

module.exports = UserService;