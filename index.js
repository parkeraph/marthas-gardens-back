const express = require('express')
const userService = require('./services/userService');
const productService = require('./services/productService');
const bodyParser = require('body-parser');
const cors = require('cors');

const baseURI = "/api"
const app = express()
const port = process.env.PORT || 3040;

//middleware
app.use(bodyParser.json());
app.use(cors());

//init services
const UService = new userService();
const PService = new productService(UService);

app.get(baseURI, (request, response) => response.status(200).json({status: "okay", version: process.env.VERSION}));

app.post(baseURI+'/user', (request, response) => { //add user no auth
    const uObj = request.body; //body is user object

    UService.addUser(uObj).then(writtenObj => {
        response.status(200).json(writtenObj);
    }).catch(error => {
        response.status(400).json({'error': error});
    })
});

app.delete(baseURI+'/user', (request, response) => { //delete one user
    const quid = request.body.quid;
    const uid = request.body.uid;
    const token = request.body.token;

    if(quid === undefined){quid = uid}; 
    if(token === undefined){response.status(400).json({'error': 'no token provided'});}
    if(uid === undefined){response.status(400).json({'error': 'no uid provided'});}

    UService.deleteUser(quid, uid, token, false).then(res => {
        response.status(200).json({"status": res.result});
    }).catch(err => {
        console.log(err);
        response.status(400).json({'error': err});
    })
})

app.put(baseURI+'/user', (request, response) => { //TODO: make UService method
    const uObj = request.body.uObj;
    const uid = request.body.uid;
    const token = request.body.token;

    if(token == undefined){response.status(400).json({'error': 'no token provided'}); return;}
    if(uid == undefined){response.status(400).json({'error': 'no uid provided'}); return;}
    if(uObj == undefined){response.status(400).json({'error': 'no uObj provided'}); return;}

    UService.deleteUser(uObj.uid, uid, token, true).then(delRes => {
        UService.addUser(uObj, uid, token).then(addRes =>{
            console.log("[UserSerive]: "+uObj.uid+"updated to: ", uObj);
            response.status(200).json({"status": addRes});
        }).catch(err => {
            response.status(200).json({"error": err});
        })
    }).catch(err => {
        response.status(200).json({"error": err});
    })
})

// Get your data: uid == quid
// Get others data: uid != quid (priv 2)
// Get all user data: quid == * (priv 2)
app.get(baseURI+'/user/', (request, response) => { 
    const uid = request.query.uid;
    let quid = request.query.quid;
    const token = request.query.token;

    if(quid != undefined){
        const reqObj = {
            uid: uid,
            quid: quid
        }

        console.log("Get user:", reqObj);
    }

    if(quid == undefined){quid = uid}; //if the request doesnt have a quid then get the userdetail from the auth uid
    if(token == undefined){response.status(400).json({'error': 'no token provided'}); return;}
    if(uid == undefined){response.status(400).json({'error': 'no uid provided'}); return;}

    UService.getUser(uid, quid, token).then(res => {
        response.status(200).json({"data": res});
    }).catch(err => {
        console.log(err);
        response.status(400).json({'error': err});
    });
})

//get products
app.get(baseURI+'/product', (request, response) => {
    const qpid = request.query.qpid;
    let qsort = request.query.qsort;

    console.log("get product", qpid, qsort);

    if(qpid == undefined){response.status(400).json({'error': 'no qpid provided'});return;}


    PService.getProduct(qpid, qsort).then(res => {
        response.status(200).json({data: res});
    }).catch(err => {
        console.log(err);
        response.status(400).json({error: err});
    })
})

//make product
app.post(baseURI+'/product/', (request, response) => {
    const pObj = request.body.pObj;
    const uid = request.body.uid;
    const token = request.body.token;

    if(pObj == undefined){response.status(400).json({'error': 'product obj not provided'});return;}
    if(uid == undefined){response.status(400).json({'error': 'uid not provided'});return;}
    if(token == undefined){response.status(400).json({'error': 'token not provided'});return;}

    PService.addProduct(pObj, uid, token).then(res => {
        response.status(200).json({"data": res});
    }).catch(err => {
        console.log("/product/ post: "+err);
        response.status(200).json({"error": err});
    })
})

app.put(baseURI+'/product/', (request, response) => {
    const pObj = request.body.pObj;
    const uid = request.body.uid;
    const token = request.body.token;

    if(pObj === undefined){response.status(400).json({'error': 'product obj not provided'});return;}
    if(uid === undefined){response.status(400).json({'error': 'uid not provided'});return;}
    if(token === undefined){response.status(400).json({'error': 'token not provided'});return;}

    PService.deleteProduct(pObj.pid, uid, token).then(res => {
        PService.addProduct(pObj, uid, token).then(addRes => {
            response.status(200).json({"data": addRes});
        }).catch(err => {
            response.status(200).json({"error": err});
        })
    }).catch(err => {
        response.status(200).json({"error": err});
    })

})

//delete product
app.delete(baseURI+'/product/', (request, response) => {
    const pid = request.body.pid;
    const uid = request.body.uid;
    const token = request.body.token;

    PService.deleteProduct(pid, uid, token).then(res => {
        response.status(200).json({"data": res.result});
    }).catch(err => {
        console.log("product delete "+err);
        response.status(400).json({"error": err});
    });
})

app.get(baseURI+"/pid/", (request, response) => {
    PService.genPID().then(res => {
        response.status(200).json({"data": {"pid": res}});
    }).catch(err => {
        response.status(200).json({"err":err});
    })
})

app.listen(port, () => console.log(`Backend listening on port ${port}`))